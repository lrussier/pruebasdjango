'''
Created on 03/09/2012

@author: Admin
'''

from clinica.Service.funciones import get_fields, filterCustomSql
from clinica.Service.login import isAuthentificated
from clinica.errorConstant import ERROR_JSON_ENTRY, ERROR_NO_AUTHENTIFICATED
from clinica.models import Personal, Calificacion
from django.core import serializers
from django.db.models.loading import get_model
from django.forms.models import modelformset_factory
from django.http import HttpResponse
from django.utils import simplejson
from django.views.decorators.csrf import csrf_exempt
import datetime
import json




@csrf_exempt
#ok version 4
def getPersonalList(request):
    #return HttpResponse(anystring)
    '''
    
    '''   
    if isAuthentificated(request): 
        #return HttpResponse("ok")
        try :  
            dictWithData=simplejson.loads(request.raw_post_data)
              
        except:
            return HttpResponse(ERROR_JSON_ENTRY)    
        #modelName is just the name so i import the model here
        myModel = get_model('clinica','CalificacionDelTratamiento')        
        tab=get_fields(myModel)                
        args=filterCustomSql(dictWithData,tab)        
        reponse=myModel.objects.filter(*args) 
        
        rep = ''
        for res in reponse :           
            califId = (res.calificacion_id)
            nb = res.cantidad
            nom = Calificacion.objects.get(pk=res.calificacion_id)
            list = Personal.objects.filter(calificacion_id=califId)
            
            while nb > 0 :
                select = str(nom)+' : '+str(nb)+'<select name="personal" id="califId_'+str(califId)+'_nb_'+str(nb)+'">'    
                for pers in list : 
                    select+='<option value="'+str(pers)+'">'+str(pers)+'</option>'               
                    
                rep+=select+'</select>'
                nb-=1  
            rep+='<br/>'
        return HttpResponse(rep)
       
    else :
        return HttpResponse(ERROR_NO_AUTHENTIFICATED)