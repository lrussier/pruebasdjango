'''
Created on 08/08/2012

@author: Megapayo
'''
from clinica.errorConstant import ERROR_USER_INACTIVE, ERROR_WRONG_PASSWORD, \
    ERROR_USER_DOES_NOT_EXIST, ERROR_JSON_ENTRY_USERnLOGGIN
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.utils import simplejson
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def isAuthentificated(request):
    
    reponse= False
    try:
        
        dictWithData=simplejson.loads(request.raw_post_data)
        
        sessionId = (dictWithData['sessionId'])
    except:
        return "no funciona"
    try : 
        session = Session.objects.get(session_key=sessionId)
    except : 
        return reponse
    uid = session.get_decoded().get('_auth_user_id')
    
    try : 
        user = User.objects.get(pk=uid)
    except ObjectDoesNotExist: 
        return reponse
    return True

@csrf_exempt
def getCsrfToken(request):
    
    '''
    Take a Json with username and password and return the sessionId associated
    implements errors
    Example :     
    {"username":"xxx","password":"xxx"}
    Return : 
    {"sessionId": "40da1ed47ddbaa696c52fd20f4e6bf48"}
    '''
    
    try:       
        
        dictWithData=simplejson.loads(request.raw_post_data)
        username = (dictWithData['username'])
        password = (dictWithData['password'])
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                #session_key = request.COOKIES[settings.SESSION_COOKIE_NAME]
                rep = request.session.session_key

                #rep=json.dumps(session_key)
                Json = '{"sessionId":"'+rep+'"}'
                return HttpResponse(Json) 
            else:
                return HttpResponse(ERROR_USER_INACTIVE)
                # Return a 'disabled account' error message
        else:
            users=User.objects.all()
            bool = False
            for u in users:
                if u.username == username:
                    bool = True
                #si el usuario existe    
                if bool :
                    return HttpResponse(ERROR_WRONG_PASSWORD)
                else :
                    return HttpResponse(ERROR_USER_DOES_NOT_EXIST)                
               
    except:
        return HttpResponse(ERROR_JSON_ENTRY_USERnLOGGIN)     
