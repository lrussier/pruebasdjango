'''
Created on 10/09/2012

@author: Admin
'''
from clinica.models import Paciente, Antecedentes
from clinica.settings import DIRR
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext, Context
import json


def setAntecedentes(request):
    '''
    Load the json "antecedentes"
    create the context send to a form page
    '''
    ## antecedentes
    json_data = open(r'C:\Users\Admin\Desktop\LaurentClinica\clinica\static\json\antecedentes.json')
    
    data1 = json.load(json_data)
    c=Context(data1)
    ## pacientes
    pacientes = Paciente.objects.all() 
    
    c['pacientes']=pacientes
    #session key for isAuthentificated function
    from django.conf import settings
    session_key = request.COOKIES[settings.SESSION_COOKIE_NAME]
    c['sessionId'] = session_key
    c['dirr'] = DIRR+'insert/antecedentes/'
    
    return render_to_response('setAntecedentes.html', c, context_instance=RequestContext(request))
   
    pass

def validateFormAntecedentes(request):
    '''
    '''
    if request.method == 'POST':
        str=''        
        pacienteId=request.POST.get('paciente')
        firmaPaciente=request.POST.get('Firma del paciente/padres o tutores')
        for x in request.POST:
            if x != 'csrfmiddlewaretoken' and x != 'paciente' and x != 'Firma del paciente/padres o tutores':   
                if request.POST.get(x, False):
                    str+=' '+x
                if request.POST.get(x) != '' and request.POST.get(x) != 'False':
                    
                    str+=' '+(request.POST.get(x))
        antecedente = Antecedentes.objects.create(stringAntecedentes=str,paciente_id=Paciente.objects.get(pk=pacienteId),firmaPaciente=firmaPaciente)
     
        antecedente.save()
        return HttpResponse("Paciente antecedentes creado")
    else:
        return HttpResponse("error Not a POST")        
    
    pass