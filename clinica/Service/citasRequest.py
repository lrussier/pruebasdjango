'''
Created on 08/08/2012

@author: Megapayo
'''

from clinica.Service.funciones import getArgs
from clinica.Service.login import isAuthentificated
from clinica.models import Cita,PersonalDeLaCita,Personal
from django.contrib.auth.models import User
from django.db.models.loading import get_model
from django.http import HttpResponse
from django.utils import simplejson
from django.views.decorators.csrf import csrf_exempt
from clinica.errorConstant import ERROR_NO_AUTHENTIFICATED





  
@csrf_exempt
#ok version 3
def setCitaWithPersonal(request):
    '''
    only 1 cita can be added here
    '''
    if isAuthentificated(request):  
        dictWithData=simplejson.loads(request.raw_post_data)                  
        tuples = (dictWithData['tuples'])
        myModel = get_model('clinica','Cita') 
        #to know how much tuple are added 
        tupleAdded=0      
        if tuples :            
            # tuple by tuple
            for tuple in tuples:
                
                #check if there is twice teh same person (which in impossible)
                personales = (dictWithData['personales'])
                list=[]
                for p in personales:
                    list.append(p['personal'])
                    
                if len(list)!=len(set(list)):
                    return HttpResponse('''{"error":"Duplicate personal"} ''')
                else:
                    
                    try :                    
                        args = getArgs(tuple,myModel)      
                        newObject = myModel.objects.create(**args) 
                    except :
                        return HttpResponse("Wrong form")
                    try : 
                        newObject.save()                    
                        tupleAdded+=1
                    except : 
                        return HttpResponse("error, not saved in database")
                    #cita is created now i have to create relations n-n of personsal with this cita
                    # and personal must exactly fit with tratamiento personal and claificacion requiered
                    try : 
                        citaId = newObject.pk
                        personales = (dictWithData['personales'])
                        
                        personAddes=0
                        
                        if personales:
                            for lista in personales:
                                try :                    
                                        
                                    newObjecto = PersonalDeLaCita.objects.create(personal=Personal.objects.get(login_id=User.objects.get(username=lista['personal'])),cita=Cita.objects.get(pk=citaId))
                                    newObjecto.save()
                                    personAddes+=1
                                except(newObjecto.error) :
                                    return HttpResponse("PersonalDeLaCita NOT CREATED ERROR"+str(personAddes))
                    except :
                        return HttpResponse("ERROR with personales list")
                        pass
                return HttpResponse(str(personAddes)+" personas added and cita created successfully")
    
    else :
        return HttpResponse(ERROR_NO_AUTHENTIFICATED)
    pass





