from Service.funciones import getFilter
from Service.login import getCsrfToken
from Service.paginaTest import testpagina, MigrationManager
from clinica.Service.citasRequest import setCitaWithPersonal
from clinica.Service.funciones import setTuples,modifyTuples, getFilterFULL
from clinica.Service.paginaTest import MigrationManager2
from clinica.Service.setAntecedentes import setAntecedentes, \
    validateFormAntecedentes
from clinica.Service.tratamientoSelectPersonal import getPersonalList
from clinica.paginas import productosConsumidos, paginaTest
from django.conf.urls import patterns, include, url
from django.contrib import admin
from mySite.adminView import loginRedirect, FormCita
from mySite.view import login_user




# Uncomment the next two lines to enable the admin:
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'clinica.views.home', name='home'),
    # url(r'^clinica/', include('clinica.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    
    (r'^logout/$', 'django.contrib.auth.views.logout',
                          {'next_page': '/index/'}),
                       
    url(r'^admin/', include(admin.site.urls)),
    url(r'^prod/',productosConsumidos),
    url(r'^paginaTest/',paginaTest),
    url(r'^test/', testpagina),
    url(r'^index/temp/', loginRedirect),
    url(r'^index/', login_user),
    url(r'^crearCita/', MigrationManager), 
    url(r'^crearPaciente/', MigrationManager2),
    url(r'^service/getPersonal/', getPersonalList),
    #formulario antecedentes validateForm
    url(r'^service/setAntecedentes/validateForm', validateFormAntecedentes),
    url(r'^service/setAntecedentes/', setAntecedentes),    
    #form pages
    url(r'^FormCita/', FormCita),
    url(r'^service/setCitaWithPersonal/',setCitaWithPersonal),  
    #to get the session ID      
    url(r'^service/login/',getCsrfToken),  
    url(r'^service/insert/(?P<modelName>.+)/$',setTuples),
    url(r'^service/modify/(?P<modelName>.+)/$',modifyTuples), 
    url(r'^service/get/(?P<modelName>.+)/$',getFilterFULL), 
    url(r'^service/(?P<modelName>.+)/$',getFilter), 
)
