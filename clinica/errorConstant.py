'''
Created on 26 juil. 2012

@author: laurent
'''
import json

ERROR_UNKWON=json.dumps({"error":"Error"})
ERROR_JSON_ENTRY=json.dumps({"error":"Bad Json entry"})
ERROR_JSON_ENTRY_USERnLOGGIN=json.dumps({"error":"Bad Json entry"})
ERROR_USER_INACTIVE=json.dumps({"error":"disabled account"})
ERROR_USER_DOES_NOT_EXIST=json.dumps({"error":"user does not exist"})
ERROR_WRONG_PASSWORD=json.dumps({"error":"wrong password"})
ERROR_USER_IS_NOT_PERSONAL=json.dumps({"error":"User is not a personal"})
ERROR_USER_IS_NOT_PERSONAL=json.dumps({"error":"User is not a personal"})
ERROR_INVALID_LOGGIN=json.dumps({"error":"Wrong login or password"})
ERROR_SESSION_ID=json.dumps({"error":"Session ID error"})
ERROR_NO_FECHAS=json.dumps({"error":"Bad JSON entry (no citas)"})
ERROR_PACIENTE_DOES_NOT_EXIST=json.dumps({"error":"Paciente does not exist"})
ERROR_TRATAMIENTO_DOES_NOT_EXIST=json.dumps({"error":"Tratamiento does not exist"})
ERROR_DATE=json.dumps({"error":"Fecha is incorect"})
ERROR_USER=json.dumps({"error":"User issue"})
ERROR_NO_AUTHENTIFICATED = json.dumps({"error":"Not authentificated"})
ERROR_QUERY = json.dumps({"error":"QUERY error"})
ERROR_SECTOR_DOES_NOT_EXIST=json.dumps({"error":"Sector does not exist"})
ERROR_NO_SECTORES=json.dumps({"error":"No Sector with this id found"})
ERROR_NO_PRODCTO=json.dumps({"error":"Product doesn't exist"})

ERROR_SAVING_DATA=json.dumps({"error":"fail at saving data, SEE IF DATAS ARE CORRECT"})
ERROR_ID_ITEM_DOES_NO_EXIST=json.dumps({"error":"No item with this id found"})
ERROR_NO_TUPLES=json.dumps({"Error":"Nothing to add"})

