'''
Created on 09/08/2012

@author: Laurent
'''


from clinica.Service.login import isAuthentificated
from clinica.errorConstant import ERROR_JSON_ENTRY, ERROR_NO_AUTHENTIFICATED, \
    ERROR_NO_TUPLES,ERROR_UNKWON
from django.core import serializers
from django.db.models.loading import get_model
from django.db.models.query_utils import Q
from django.http import HttpResponse
from django.utils import simplejson
from django.views.decorators.csrf import csrf_exempt
import datetime
import json






def get_fields(modelName):
    ''' 
    Make a list of field.name.
    Note: if the field name is a foreign key, django automaticaly add "_id" to the field name,
    but the purpose of is to get exact field name for custom SQL, so use db_column in model
    
    '''
    
    ########################################################################################################
    ###################### https://docs.djangoproject.com/en/dev/ref/models/fields/  #######################
    ########################################################################################################
    #Behind the scenes, Django appends "_id" to the field name to create its database column name.
    #In the above example, the database table for the Car model will have a manufacturer_id column.
    #(You can change this explicitly by specifying db_column) However, your code should never have to deal
    #with the database column name, unless you write custom SQL. You'll always deal with the field names of
    # your model object.
    ########################################################################################################
    
    return [field.name for field in modelName._meta.fields]

def filterCustomSql(dictWithData,tab): 
    '''
    CUSTOM QUERY
    
    if filtro[0] then is a specifique value
    else if filtro [1] it's a range between the 2 values
    else it take all values
    
    if it's a range the second value must be higher
    '''   
    #Custom SQL   
    args=[]
    for item in tab:
        try : 
            dictWithData['filtro'][0][item]
            #TODO : type checking ?            
            if (dictWithData['filtro'][0][item] != ''):                       
                idJson = (dictWithData['filtro'][0][item]) 
                p=(Q(**{item:idJson}))                                 
                try : 
                    idJson2 = (dictWithData['filtro'][1][item])
                    ## the ** allows item to be a variable 
                    p=Q(**{item+'__range':[idJson,idJson2]})
                    args.append(p)
                except : 
                    args.append(p)
        except :
            pass
    return args
    
@csrf_exempt
#ok version 4
def getFilter(request,modelName):
    #return HttpResponse(anystring)
    '''
    If you want to search a range, the first value MUST be lower then the second one
    take JSON  like :
    
    **1**
    to return all values take : 
    {"sessionId":"3924d0b6e701a68c20e92c8b6f4fdf7d"}    
    return :     
    {"result":[{"nombre": "pepito"}, {"nombre": "1effff3"}, {"nombre": "sector 1"}, {"nombre": "sector 2"}]}
    
    **2**
    to get specifics values take :
    {"sessionId":"3924d0b6e701a68c20e92c8b6f4fdf7d","filtro":[{"nombre":"sector 1"}]}
    return :
    {"result":[{"nombre": "sector 1"}]}
    
    **3**
    to get range between 2 values take :
    {"sessionId":"3924d0b6e701a68c20e92c8b6f4fdf7d","filtro":[{"nombre":"qq"},{"nombre":"zz"}]}
    return :
    {"result":[{"nombre": "sector 1"}, {"nombre": "sector 2"}]}
    
    
    
    CUSTOM QUERY
    
    if filtro[0] then is a specifique value
    else if filtro [1] it's a range between the 2 values
    else it take all values
    
    if it's a range the second value must be higher
    '''   
    if isAuthentificated(request): 
        try :  
            dictWithData=simplejson.loads(request.raw_post_data)
              
        except:
            return HttpResponse(ERROR_JSON_ENTRY)    
        #modelName is just the name so i import the model here
        myModel = get_model('clinica',modelName)        
        tab=get_fields(myModel)
                        
        args=filterCustomSql(dictWithData,tab)    
          
        reponse=myModel.objects.filter(*args) 
        
        ################### SERIALIZATION ########################
        raw_data = serializers.serialize('python', reponse)
        
        # now extract the inner `fields` dicts
        
        #return HttpResponse(rep)
            
        #
        #return HttpResponse(rep)
        
        # and now dump to JSON
        dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else None
        actual_data = [d['fields'] for d in raw_data]
        output = json.dumps(actual_data,default=dthandler)                   
        Json = "{\"result\":"+output+"}"                    
        return HttpResponse(Json) 
        
    else :
        return HttpResponse(ERROR_NO_AUTHENTIFICATED)


@csrf_exempt
#ok version 4
def getFilterFULL(request,modelName):
    #return HttpResponse(anystring)
    '''
    Same as getFilter but include id
    '''   
    if isAuthentificated(request): 
        try :  
            dictWithData=simplejson.loads(request.raw_post_data)
              
        except:
            return HttpResponse(ERROR_JSON_ENTRY)    
        #modelName is just the name so i import the model here
        myModel = get_model('clinica',modelName)        
        tab=get_fields(myModel)
                        
        args=filterCustomSql(dictWithData,tab)    
          
        reponse=myModel.objects.filter(*args) 
        
        ################### SERIALIZATION ########################
        raw_data = serializers.serialize('python', reponse)        
        dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else None        
        output = json.dumps(raw_data,default=dthandler)                   
        Json = "{\"result\":"+output+"}"                    
        return HttpResponse(Json) 
        
    else :
        return HttpResponse(ERROR_NO_AUTHENTIFICATED)
    
        
def getArgs (tuple,myModel):
    '''
    return a maping 
    associating field in tuple and field in fields
    tuple length is is 1 smaller then fields length cause of the id field
    id is automatically generated, you don't have to insert it
    
    TODO : check if a field can be null in order to improve the length test
    '''   
    fields=get_fields(myModel) 
    #Custom SQL   
    args={}
    #The lists must have the SAME LENGTH +1 because id is automaticaly created
    #AND must have the SAME NAME
    
    if fields.__len__() == (tuple.__len__()+1):        
        for field in fields :
            if field <> 'id' :
                #if it's a foreignKey
                if myModel._meta.get_field(field).get_internal_type() == 'ForeignKey' or myModel._meta.get_field(field).get_internal_type() == 'OneToOneField':
                    foreignKeyModel =  myModel._meta.get_field(field).rel.to
                    #raise an error if the object doesnt exist
                    args[field]= foreignKeyModel.objects.get(pk=tuple[field])
                #################################################################################
                #THE FORMSET IS ALREADY CHECKED cf : formset.is_valid() BUT WHEN A FIELD 
                #WITH BLANK=TRUE AND NULL=TRUE SUBMITED ITS LIKE '' WITCH IS A STRING SO INVALID
                #THAT .isdigit() IS CALLED
                #################################################################################    
                else :#TODO : improvie algoritmia here it can obviously be improve but my brain is not responding                  
                    if (myModel._meta.get_field(field).get_internal_type() == 'PositiveIntegerField') or (myModel._meta.get_field(field).get_internal_type() == 'DecimalField'):
                        if not (tuple[field].isdigit()):
                            args[field]=None
                        else : 
                            args[field]=tuple[field] 
                    else : 
                        args[field]=tuple[field]      
                            
                                  
        return args
    else : 
        return {'ERROR NUMBER OF FIELDS ARE WRONG':'ERROR NUMBER OF FIELDS ARE WRONG'}
    
    pass


@csrf_exempt
def setTuples(request,modelName):
    
    '''
    take JSON  like :
    {"sessionId":"8c4bba1e60cf2a5dd5aff229b334b786",
    "tuples":[{"nombre":"espeTest"},{...}]}
    
    return : 
    {"success":"x tuples added successfully"}
    OR
    Errors : see errorConstant.py
    
    
    ''' 
      
    if isAuthentificated(request):   
        myModel = get_model('clinica',modelName)        
       
        
        dictWithData=simplejson.loads(request.raw_post_data)                  
        tuples = (dictWithData['tuples'])
        
        
        
        #to know how much tuple are added 
        tupleAdded=0      
        if tuples :
            
            # tuple by tuple
            for tuple in tuples:
                
                args = getArgs(tuple,myModel)
                
                #return HttpResponse(args['cita_id'])
                #creation and saving in DB
                try :
                    newObject = myModel.objects.create(**args) 
                
                except :
                    return HttpResponse("object create doesnt worked(fields arn't right) OR Tuple is unique and already exist"  )
                
                try : 
                    newObject.save()
                    tupleAdded+=1
                except : 
                    return HttpResponse(ERROR_UNKWON)
            return HttpResponse('''{"success":"'''+str(tupleAdded)+" tuples added successfully"+'''"}''')            
        else : 
            return HttpResponse(ERROR_NO_TUPLES)
    else :
        return HttpResponse(ERROR_NO_AUTHENTIFICATED)
    
@csrf_exempt
def setTuplesFromJson(json,modelName):
    
    '''
    take JSON  like :
    {"sessionId":"8c4bba1e60cf2a5dd5aff229b334b786",
    "tuples":[{"nombre":"espeTest"},{...}]}
    
    return : 
    {"success":"x tuples added successfully"}
    OR
    Errors : see errorConstant.py
    
    ''' 
   
       
    myModel = get_model('clinica',modelName) 
    dictWithData=simplejson.loads(json)  
    tuples = (dictWithData['tuples'])
    #to know how much tuple are added 
    tupleAdded=0   
      
    if tuples :
        
        # tuple by tuple
        for tuple in tuples: 
            fields=get_fields(myModel) 
            
            args = getArgs(tuple,myModel)
            
            #return HttpResponse(args['fechaDeNacimiento'])
            #creation and saving in DB
            
                
            newObject = myModel.objects.create(**args) 
           
            
            try : 
                newObject.save()
                tupleAdded+=1
            except : 
                return HttpResponse(ERROR_UNKWON)
        return HttpResponse('''{"success":"'''+str(tupleAdded)+" tuples added successfully"+'''"}''')         
    else : 
        return HttpResponse(ERROR_NO_TUPLES)
   
       

def getArgsWithId (tuple,myModel):
    '''
    return a maping 
    associating field in tuple and field in fields
    tuple length is equlas then fields length 
    
    TODO : check if a field can be null in order to improve the length test
    '''   
    fields=get_fields(myModel) 
    #Custom SQL   
    args={}
    #The lists must have the SAME NAME
    
    if fields.__len__() == tuple.__len__():        
        for field in fields :
            if field == 'id' :
                myModel.objetcs.get(id=tuple[field])
                pass
            else :
                #if it's a foreignKey
                if myModel._meta.get_field(field).get_internal_type() == 'ForeignKey':
                    foreignKeyModel =  myModel._meta.get_field(field).rel.to
                    #raise an error if the object doesnt exist
                    args[field]= foreignKeyModel.objects.get(pk=tuple[field])
                    
                else :                   
                
                    args[field]=tuple[field]                
        return args
    else : 
        return {'ERROR NUMBER OF FIELDS ARE WRONG':'ERROR NUMBER OF FIELDS ARE WRONG'}
    
    pass
@csrf_exempt
# no funciona
def modifyTuples(request,modelName):
    
    '''
    Same as setTuples with in addtion the Id and an error if item doesn't exist
    See .../get/model to get full Json with id
    take JSON  like :
       {"sessionId":"54ce7d32ad0c8264ce76e04670f5a9f9","tuples":
       [{"id":"2","fields":[{"nombre":"1effff3"}]},{"id":"1","fields":[{"nombre":"pepito"}]}]}
       
       return : 
    {"success":"x tuples modified successfully"}
    OR
    Errors : see errorConstant.py
    
    '''
    
    if isAuthentificated(request):  
        try : 
            myModel = get_model('clinica',modelName)  
            
            dictWithData=simplejson.loads(request.raw_post_data) 
                      
            tuples = (dictWithData['tuples'])
            
        except:
            return HttpResponse(ERROR_JSON_ENTRY)
        
       
        #to know how much tuple are modified
        tupleModified=0      
        if tuples :
           
            # tuple by tuple
            for tuple in tuples:
                   
                
                try : 
                    Object = myModel.objects.get(pk = tuple['id']) 
                 
                except:
                    return HttpResponse("WRONG ID("+tuple['id']+"), object doesn't exist")
                
                
                for  x in tuple['fields']:
                    
                                    
                    #return HttpResponse(x.__len__())
                    fields=get_fields(myModel) 
                    
                    for field in fields :  
                        if field <> 'id' and x[field] <> '':  
                                               
                            #if it's a foreignKey
                            if myModel._meta.get_field(field).get_internal_type() == 'ForeignKey' or myModel._meta.get_field(field).get_internal_type() == 'OneToOneField':
                                foreignKeyModel =  myModel._meta.get_field(field).rel.to
                                #raise an error if the object doesnt exist
                                setattr(Object, field, foreignKeyModel.objects.get(pk=x[field]))
                                
                            #################################################################################
                            #THE FORMSET IS ALREADY CHECKED cf : formset.is_valid() BUT WHEN A FIELD 
                            #WITH BLANK=TRUE AND NULL=TRUE SUBMITED ITS LIKE '' WITCH IS A STRING SO INVALID
                            #THAT .isdigit() IS CALLED
                            #################################################################################    
                            else :#TODO : improvie algoritmia here it can obviously be improve but my brain is not responding                  
                                if (myModel._meta.get_field(field).get_internal_type() == 'PositiveIntegerField') or (myModel._meta.get_field(field).get_internal_type() == 'DecimalField'):
                                    if not (x[field].isdigit()):
                                        
                                        setattr(Object, field, None )
                                    else : 
                                        setattr(Object, field, x[field] )
                                else : 
                                    '''
                                    http://stackoverflow.com/questions/763558/django-object-get-set-field
                                    Object.field doesnt work because field is not the variable but an attribute that doesnt existe
                                    '''
                                    setattr(Object, field, x[field] )
                                            
                      
                        Object.save()
                    '''except :
                        return HttpResponse("object create doesnt worked(fields arn't right)")
                    '''
               
                    tupleModified+=1
                
                
                return HttpResponse('''{"success":"'''+str(tupleModified)+" tuples modified successfully"+'''"}''')          
        else : 
            return HttpResponse(ERROR_NO_TUPLES)
    else :
        return HttpResponse(ERROR_NO_AUTHENTIFICATED)
    


