'''
Created on 31/07/2012

@author: Megapayo
'''




from clinica.models import ProductosDelTratamiento, Producto
from django.db.models.query_utils import Q
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext



import json





def productosConsumidos(request):
    
    productos = ProductosDelTratamiento.objects.all()
    prod=[]
    for p in productos:    
        num=p.producto.referencia
        can=str(p.cantidad)
        prod.append(num)
        prod.append(can)
    #les produits du traitements
    rep = json.dumps(prod)
    
    prod= Producto.objects.all()
    prodd=[]
    for p in prod:    
        num=p.referencia
        can=str(p.quantidad)
        prodd.append(num)
        prodd.append(can)
    rep2 = json.dumps(prodd)
    
    repFinal='productos a consumir : '+rep+'<br/>productos stock : '+rep2
    return HttpResponse(repFinal)  
    
def paginaTest(request):
    
    return render_to_response('paginaTest.html', context_instance=RequestContext(request))
    
    pass 
    
    