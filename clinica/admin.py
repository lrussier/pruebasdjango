'''
Created on 26 juil. 2012

@author: laurent
'''
from clinica.models import Personal, Clinica, JefeClinica, personalDelSector, \
    Sector, personalDeLaEspecialisacion, Tratamiento, especialidad, Producto, Gastos, \
    Cita, Paciente, CalificacionDelTratamiento, ProductosDelTratamiento, Factura, \
    Calificacion, Message, Historial, Files, FilesConHistorial, PacienteOrtodonlogia, \
    PersonalDeLaCita, Antecedentes
from django.contrib import admin



class historialInLine(admin.TabularInline):
    model = Historial
    extra = 1
class AntecedentesInLine(admin.TabularInline):
    model = Antecedentes
    extra = 1

class pacienteAdmin(admin.ModelAdmin):
    inlines = (historialInLine,AntecedentesInLine)
    pass

class personalInLine(admin.TabularInline):
    
    model = Personal
    extra = 1

class clinicaAdmin(admin.ModelAdmin):
    inlines = (personalInLine,)
    pass

class personalDelSectorAdmin(admin.TabularInline):
    model = personalDelSector
    extra = 1
    
class sectorAdmin(admin.ModelAdmin):
    inlines = (personalDelSectorAdmin,)
    
class personalDeLaEspecialisacionAdmin(admin.TabularInline):
    model = personalDeLaEspecialisacion
    extra = 1
    
class calificacionDelTratamientoAdmin(admin.TabularInline):
    model = CalificacionDelTratamiento
    extra = 1
    
class productosDelTratamientoAdmin(admin.TabularInline):
    model = ProductosDelTratamiento
    extra = 1
    
class personalDeLaCitaAdmin(admin.TabularInline):
    model = PersonalDeLaCita
    extra = 1


class tratamientoAdmin (admin.ModelAdmin):
    inlines = (calificacionDelTratamientoAdmin,productosDelTratamientoAdmin)


  
class citaAdmin (admin.ModelAdmin):
    inlines = (personalDeLaCitaAdmin,)
    pass

class facturaAdmin(admin.ModelAdmin):
    list_display = ('precioSinIVA','precioConIVA','fecha')
    pass

class messageAdmin(admin.ModelAdmin):
    list_filter = ('importancia', 'receptor_id','emisor_id')
    pass

class FilesConHistoriallInLine(admin.TabularInline):
    
    model = FilesConHistorial
    extra = 1

class historialAdmin(admin.ModelAdmin):
    inlines = (FilesConHistoriallInLine,)
    pass
admin.site.register(Personal)
admin.site.register(JefeClinica)


admin.site.register(Sector,sectorAdmin)
admin.site.register(Clinica,clinicaAdmin)
admin.site.register(Tratamiento,tratamientoAdmin)
admin.site.register(especialidad)
admin.site.register(Producto)
admin.site.register(Message,messageAdmin)
admin.site.register(Gastos)
admin.site.register(Cita,citaAdmin)
admin.site.register(Paciente,pacienteAdmin)
admin.site.register(Factura,facturaAdmin)
admin.site.register(Calificacion)
admin.site.register(Historial,historialAdmin)
admin.site.register(Files)
admin.site.register(Antecedentes)

admin.site.register(PacienteOrtodonlogia)
