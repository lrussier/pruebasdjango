'''
Created on 09/08/2012

@author: Megapayo
'''

from clinica.Service.funciones import setTuplesFromJson
from clinica.models import CitaForm, Cita, Paciente, Tratamiento, PacienteForm

from django.forms.formsets import formset_factory
from django.http import HttpResponse

from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def testpagina(request):
    if request.method=='POST':
        return HttpResponse( request.body)
        #return HttpResponse( request.POST['mutable'])
        #dictWithData=simplejson.loads(request.raw_post_data)
        return HttpResponse("ok post")
    return HttpResponse("pas post")



def testCrearCita(request):
    
    if request.method == "POST":
        
        
        CitaFormSet = formset_factory(CitaForm) 
        formset = CitaFormSet(request.POST) 
        if formset.is_valid() :  
            try : 
                idT=formset[0]['tratamiento_id'].value()
                idP=formset[0]['paciente_id'].value()
                if formset[0]['fecha'].value() != '':
                    fechaA=formset[0]['fecha'].value()
                else :
                    fechaA=None
                com=formset[0]['comentario'].value()
                #return HttpResponse(idP)
                cita =  Cita.objects.create(tratamiento_id=Tratamiento.objects.get(pk=idT),paciente_id=Paciente.objects.get(pk=idP),fecha=fechaA,comentario=com)
                cita.save()
            except : 
                return HttpResponse('bad form')
        #val=formset.cleaned_data[0]['paciente_id']
        
        #return HttpResponse(formset[0])
            return HttpResponse('cita saved')
              
        try:                        
            formset.is_valid()            
        except : 
                      
            return HttpResponse(formset.errors)
        
        
        
        return HttpResponse("error saved") 
        #return HttpResponse("error saved")
        #test=formset['form-0-paciente_id']
        #return HttpResponse(test)
        
    return request.body

def testCrearPaciente(request):
    '''
    only to add 1 paciente
    
    '''
    PacienteFormSet = formset_factory(PacienteForm)
    
    if request.method == "POST":
        
        formset = PacienteFormSet(request.POST)
        
        
        
        
        if formset.is_valid() :
            # do something with the formset.cleaned_data
            
            formCleaned = formset.cleaned_data
            
            
           
            json = '{"sessionId":"'+request.POST['sessionId']+'","tuples":[{'
            for u in  formCleaned[0]:
                json+='"'+u+'":"'+str(formset[0][u].value())+'",'
            #remove the last character that is a ","   
            json = json[:-1]
            json+='}]}'
            
            #return HttpResponse(json)
            return setTuplesFromJson(json,'Paciente')
        else:
            return HttpResponse(formset.errors)
    else:
        return HttpResponse("not a post")       
     

def MigrationManager(request):
    if request.method == 'POST': 
        return HttpResponse(testCrearCita(request))
    else :
        return HttpResponse("not a post")
    
def MigrationManager2(request):
    if request.method == 'POST': 
        return HttpResponse(testCrearPaciente(request))
    else :
        return HttpResponse("not a post")
        
   
    
    