'''
Created on 26 juil. 2012

@author: laurent
'''
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.db import models

from django.db.models.signals import post_save
from django.forms import ModelForm

from settings import PRIVATE_DIR

import os.path



################## files settings   ###################

fs = FileSystemStorage(location=PRIVATE_DIR)

#######################################################
#Informaciones basicas de las personas (jefe, personal, pacientes)
class CommonInfoPerson(models.Model):    
    nombre = models.CharField(max_length=30)
    nombre2 = models.CharField(max_length=30,blank=True, null=True)
    apellido = models.CharField(max_length=30)
    apellido2 = models.CharField(max_length=30,blank=True, null=True)
    fechaDeNacimiento = models.DateField()
    #TODO : age should not be editable ?    
    age = models.PositiveIntegerField(blank=True, null=True)
    domocilio = models.CharField(max_length=140,blank=True, null=True)
    localidad = models.CharField(max_length=50,blank=True, null=True)
    codigoPostal = models.DecimalField(max_digits=5, decimal_places=0,blank=True, null=True)
    telFijo = models.DecimalField(max_digits=13, decimal_places=0,blank=True, null=True)
    telMob = models.DecimalField(max_digits=13, decimal_places=0,blank=True, null=True)
    correo = models.EmailField(max_length=75,blank=True, null=True)
    
    #override save function to calculate the age thanks teh birthdate(fechaDeNacimiento) 
    def save(self):
        
        from datetime import date
        days_in_year = 365.25
        self.age  = int((date.today() - self.fechaDeNacimiento).days/days_in_year)
        
        super(CommonInfoPerson, self).save()
    
    class Meta:
        abstract = True
#######################################################
class Object():
    def __init__(self, d):
        for a, b in d.items():
            if isinstance(b, (list, tuple)):
                setattr(self, a, [Object(x) if isinstance(x, dict) else x for x in b])
            else:
                setattr(self, a, Object(b) if isinstance(b, dict) else b)

class JefeClinica(CommonInfoPerson):
    #es el objeto de user ( relacion 1-1)
    login_id = models.ForeignKey(User,db_column='login_id')
    
    def __str__(self):
        return self.nombre
    pass


class Clinica(models.Model):
    nombre = models.CharField(max_length=30)
    jefe = models.OneToOneField(User)
    direccion = models.CharField(max_length=70)
    
    def __str__(self):
        return "Clinica : "+str(self.nombre)
    pass

BOOL_CHOICES_EamenFisico = ((True, 'Normal'), (False, 'Alterado'))
BebidasChoice = (
            ('0','Agua'),
            ('1','Gaseosa o jugo con azucar'),
            ('2','Idem diet'),
            )

#ortodonlogia
tecnicaDeCipillado = (
            ('0','Sin tecnica'),
            ('1','Bass'),
            ('2','Circular'),
            ('3','Barrido'),
            ('4','Otra'),
            )
interDental = (
            ('0','No usa'),
            ('1','Hilo o cinta'),
            ('2','palillo'),
            ('3','cepillito'),
            ('4','Otro'),
            )

     
class Paciente(CommonInfoPerson):
    #fechaDeIngreso = models.DateField(auto_now=False, auto_now_add=True,blank=True, null=True)    
    NIF = models.CharField(max_length=30,blank=True, null=True)
    
    #should be automaticaly selected with the same clinica id of the personal recording it
    clinica_id = models.ForeignKey(Clinica,db_column='clinica_id')
    CardioVasculares = models.BooleanField(default=False)    
    QuienCardio = models.CharField(max_length=50,blank = True, null = True)    
    Diabetes = models.BooleanField(default=False)
    QuienDiabetes = models.CharField(max_length=50,blank = True, null = True)
    Cancer = models.BooleanField(default=False)
    QuienCancer = models.CharField(max_length=50,blank = True, null = True)
    Otro = models.BooleanField(default=False)
    QuienOtro = models.CharField(max_length=50,blank = True, null = True) 
    Aleria_a = models.CharField(max_length=70,blank = True, null = True) 
    Tipo = models.CharField(max_length=70,blank = True, null = True) 
    TratamientoMedico = models.BooleanField(default=False)
    MedicacionParaLaUrgencia = models.CharField(max_length=300,blank = True, null = True) 
    ExamenFisicoGeneral = models.BooleanField(choices=BOOL_CHOICES_EamenFisico,default=True)
    EspecificarAlteracion = models.CharField(max_length=100,blank = True, null = True)
    Fuma = models.BooleanField(default=False)
    TomaAlcoholDiariamente = models.BooleanField(default=False)
    BebidasDiarias = models.CharField(choices=BebidasChoice,max_length=100,blank = True, null = True)
    MotivoDeConsulta = models.CharField(max_length=300,blank = True, null = True)
    #ANTECEDENTES TRING
    
    #ortodonlogia = models.OneToOneField(PacienteOrtodonlogia,unique=True) 
    def __str__(self):
        return "Paciente : "+str(self.nombre)+'(NIF : '+str(self.NIF)+')'
    #abstract parent so
    #http://semi-legitimate.com/blog/item/django-save-got-an-unexpected-keyword-argument-forceinsert
    def save(self, *args, **kwargs):
        
        super(CommonInfoPerson, self).save(*args, **kwargs)
    pass

class Antecedentes(models.Model):
    paciente_id = models.OneToOneField(Paciente,db_column='paciente_id')
    stringAntecedentes = models.CharField(max_length=1000,blank = True, null = True)
    firmaPaciente = models.CharField(max_length=30,blank = True, null = True)
    aclaracion = models.CharField(max_length=30,blank = True, null = True)
    firmaAlumno = models.CharField(max_length=30,blank = True, null = True)
    firmaDocente = models.CharField(max_length=30,blank = True, null = True)
    pass
class PacienteOrtodonlogia(Paciente):
    TecnicaDeCepillado = models.CharField(choices=tecnicaDeCipillado,max_length=30,blank = True,null = True)
    InterDental = models.CharField(choices=interDental,max_length=30,blank = True, null = True)
    AntesDelDesayuno = models.BooleanField(default=False)
    DespuesDelDesayuno = models.BooleanField(default=False)
    DespuesDeAlmuerzo = models.BooleanField(default=False)
    DespuesDeCena = models.BooleanField(default=False)
    Otra = models.BooleanField(default=False)
    pass 
class Calificacion(models.Model):
    calificacion = models.CharField(max_length=30, unique= True)
    def __str__(self):
        return self.calificacion
    pass

class Personal(CommonInfoPerson):
    #un user de la base de admin ( relacion 1-1)
    login_id = models.ForeignKey(User, unique = True,db_column='login_id')
    #calificacion : Doctor, Enfermera, Asistante...
    calificacion_id = models.ForeignKey(Calificacion,db_column='calificacion_id')
    
    clinica_id = models.ForeignKey(Clinica,db_column='clinica_id')
    
    def __str__(self):
        return self.login_id.__str__()
    pass

#sector : cirugia, dentista...   
class Sector(models.Model):
    #id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    doctores = models.ManyToManyField(Personal, through='personalDelSector')
    def __str__(self):
        return self.nombre
    pass



## (n-n) nueva tabla
class personalDelSector(models.Model):
    class Meta:
        unique_together = ['personal','sector']  
        
    personal = models.ForeignKey(Personal)
    sector = models.ForeignKey(Sector)
    fechaDeEntrada = models.DateField() 
    
    pass

class especialidad(models.Model):
    nombre = models.CharField(max_length=30)
    sector_id = models.ForeignKey(Sector,db_column='sector_id')
    doctores = models.ManyToManyField(Personal, through='personalDeLaEspecialisacion')
    def __str__(self):
        return str(self.nombre)+' ('+str(self.sector_id)+')'
    pass

## (n-n) nueva tabla
class personalDeLaEspecialisacion(models.Model):
    class Meta:
        unique_together = ['personal','especialidad']  
        
    personal = models.ForeignKey(Personal)
    especialidad = models.ForeignKey(especialidad)   
    pass



class Producto(models.Model):
    nombre = models.CharField(max_length=30)
    referencia = models.CharField(max_length=30)
    cantidad = models.DecimalField(max_digits=5, decimal_places=0)
    nivelAlerta = models.DecimalField(max_digits=5, decimal_places=0)
    NivelCritico = models.DecimalField(max_digits=5, decimal_places=0)
    def __str__(self):
        return str(self.nombre)+' ('+str(self.referencia)+') Cantidad : '+str(self.cantidad)
    pass

class Tratamiento(models.Model):
    clinica_id = models.ForeignKey(Clinica,db_column='clinica_id')
    especialidad = models.ForeignKey(especialidad)
    nombre = models.CharField(max_length=30)
    duracion = models.DecimalField(max_digits=4, decimal_places=2)
    calificacion = models.ManyToManyField(Calificacion, through='CalificacionDelTratamiento')
    productos = models.ManyToManyField(Producto, through='ProductosDelTratamiento')    
    def __str__(self):
        return self.nombre
    pass



class Cita(models.Model):
    
    paciente_id =  models.ForeignKey(Paciente,db_column='paciente_id')
    tratamiento_id = models.ForeignKey(Tratamiento,db_column='tratamiento_id')
    personal = models.ManyToManyField(Personal, through='PersonalDeLaCita')
    fecha = models.DateTimeField(blank=True, null=True)
    comentario = models.CharField(max_length=200,blank=True, null=True)
    def __str__(self):
        return 'cita de '+str(self.paciente_id)+' para '+str(self.tratamiento_id)
    
    pass

## (n-n) nueva tabla
class PersonalDeLaCita(models.Model):
    class Meta:
        unique_together = ['personal','cita']   
    personal = models.ForeignKey(Personal)    
    cita = models.ForeignKey(Cita) 
    
     
    pass

    

## (n-n) nueva tabla
class CalificacionDelTratamiento(models.Model):
    class Meta:
        unique_together = ['calificacion','tratamiento']  
    calificacion = models.ForeignKey(Calificacion)    
    tratamiento = models.ForeignKey(Tratamiento)
    cantidad = models.PositiveIntegerField(default=1)
     
    pass

## (n-n) nueva tabla
class ProductosDelTratamiento(models.Model):
    class Meta:
        unique_together = ['producto','tratamiento']  
    producto = models.ForeignKey(Producto)
    tratamiento = models.ForeignKey(Tratamiento) 
    cantidad = models.DecimalField(max_digits=5, decimal_places=0, default=1)  
    
    
    pass
def do_something(sender, **kwargs):
    # the object which is saved can be accessed via kwargs 'instance' key.
    obj = kwargs['instance']
    can = obj.cantidad
    idd = obj.producto_id
    elProducto = Producto.objects.get(id=idd)
    elProducto.cantidad-=can
    elProducto.save()
    
    # ...do something else...
#post_save.connect(do_something, sender=ProductosDelTratamiento)

def quitar_producto(sender, **kwargs):
    #verificar si las cantidadesdel Save son diferentes
    if kwargs['created']:
        
        
        
        cita = kwargs['instance']
        tratId = cita.tratamiento_id
        productosDelTratamiento = ProductosDelTratamiento.objects.filter(tratamiento_id=tratId)
        for producto in productosDelTratamiento:
            idd=producto.producto_id
            can=producto.cantidad
            elProducto = Producto.objects.get(id=idd)
            enlace="http://localhost:8000/admin/clinica/producto/"+str(idd)
            #hay suficientes productos
            if elProducto.cantidad>=can:
                elProducto.cantidad-=can
                
                
                elProducto.save()
                if elProducto.cantidad<=elProducto.NivelCritico:
                    message = Message.objects.create(receptor_id=User.objects.get(username='admin'),importancia=4,text="Nivel de ALERTA CRITICA del producto alcanzado",link=enlace)   
                    message.save()
                elif elProducto.cantidad<=elProducto.nivelAlerta:
                    message = Message.objects.create(receptor_id=User.objects.get(username='admin'),importancia=1,text="Nivel de alerta del producto alcanzado",link=enlace)   
                    message.save()
                
                    
                
            else:
                # enviar mesage de alterta 
               
                message = Message.objects.create(receptor_id=User.objects.get(username='admin'),importancia=5,text="ERROR : La cita no ha sido creada, las cantidades de un producto superan al stock disponible",link=enlace)   
                message.save()
                
        
    pass


####### Here is used the function when a cita is created
post_save.connect(quitar_producto, sender=Cita)

class Message(models.Model):
    receptor_id = models.ForeignKey(User, related_name='receptor',db_column='receptor_id')
    emisor_id = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL, related_name='emisor',db_column='emisor_id')
    fecha = models.DateTimeField(auto_now_add=True, blank=True)
    importancia = models.PositiveIntegerField(default=0)
    text = models.CharField(max_length=300)
    link = models.URLField(max_length=100, null=True)
    def __str__(self):
        return 'Mesage para : '+str(self.receptor_id)+' ('+str(self.importancia)+')'
    
    pass

class Gastos(models.Model):
    precio = models.DecimalField(max_digits=8, decimal_places=2)
    numero = models.CharField(max_length=30)
    producto_id = models.ForeignKey(Producto,db_column='producto_id')
    provedor =  models.CharField(max_length=30)
    
    def __str__(self):
        return str(self.numero)+' de '+str(self.precio)+' e'
    pass


class Factura(models.Model):
    precioSinIVA = models.CharField(max_length=10)
    IVA = models.CharField(max_length=5, default=1.18, editable=False)
    precioConIVA = models.CharField(max_length=10, editable=False )
    fecha = models.DateField()
    cita_id = models.ForeignKey(Cita,db_column='cita_id')
    def precioConIVA(self):
        return  str(self.precioSinIVA)
    
    pass

class Historial(models.Model):
    paciente_id =  models.ForeignKey(Paciente,db_column='paciente_id')
    tratamiento_id = models.ForeignKey(Tratamiento,default=None,blank = True, null=True,db_column='tratamiento_id')
    fecha = models.DateTimeField(blank=True, null=True)
    comentario = models.CharField(max_length=200,blank=True, null=True)
    pass
#When i create a cita, witch means a treatment for a pacient, I add it to the historial table
def crearHistorial(sender, **kwargs):
    
    if kwargs['created']:        
        cita = kwargs['instance']
        tratIdZ = cita.tratamiento_id
        fechaZ = cita.fecha
        paciente_idZ = cita.paciente_id
        comentarioZ = cita.comentario        
        historial = Historial.objects.create(paciente_id=paciente_idZ,tratamiento_id=tratIdZ,fecha=fechaZ,comentario=comentarioZ)
        historial.save()
    pass

post_save.connect(crearHistorial, sender=Cita)

class Files(models.Model):
    
    file = models.FileField(upload_to='files', storage=fs)
    
    def filename(self):
        return os.path.basename(self.file.name)    
    def __str__(self):        
        return self.filename()
    pass
    
## (n-n) nueva tabla
class FilesConHistorial(models.Model):
    class Meta:
        unique_together = ['file','historial']  
        
    file = models.ForeignKey(Files)
    historial = models.ForeignKey(Historial)
    tipo = models.CharField(max_length=20,blank=True, null=True) 
    
    pass
############################# model Form #################################

class CitaForm(ModelForm):
    class Meta:
        model = Cita
        '''
    def save(self):
        cita = Cita(paciente_id=self.cleaned_data['paciente_id'],tratamiento_id=self.cleaned_data['tratamiento_id'],fecha=self.cleaned_data['fecha'])
        cita.save()
        return cita 
        '''
    
class PacienteForm(ModelForm):
    class Meta:
        model = Paciente
    def clean(self):
        self.saved_data=self.cleaned_data
        return self.cleaned_data
    '''def save(self):
        #cita = Cita(paciente_id=self.cleaned_data['paciente_id'],tratamiento_id=self.cleaned_data['tratamiento_id'],fecha=self.cleaned_data['fecha'])
        cita.save()
        return cita  '''  
    
        

