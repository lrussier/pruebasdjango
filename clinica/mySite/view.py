'''
Created on 09/08/2012

@author: Megapayo
'''

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt



@csrf_exempt
def login_user(request):
    '''
    http://solutoire.com/2009/02/26/django-series-1-a-custom-login-page/
    '''
    state = "Please log in below..."
    username = password = ''
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                
                return HttpResponseRedirect('temp/')
                
            else:
                state = "Your account is not active, please contact the site admin."
                return render_to_response('auth.html',{'state':state, 'username': username})
        else:
            state = "Your username and/or password were incorrect."

    return render_to_response('auth.html',{'state':state, 'username': username})
        
