'''
Created on 09/08/2012

@author: Megapayo
'''

from clinica.models import Cita, Paciente
from clinica.settings import DIRR
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.forms.models import modelformset_factory
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt



@csrf_exempt
def loginRedirect(request):
    #TODO : GET usrname and group with sessionID
    #session_key = request.COOKIES[settings.SESSION_COOKIE_NAME]
    username = password = ''
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        state=''
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                miUser=User.objects.get(username=username)
                # i assume that our user only have 1 group, jefe de clinica o personal
                try : 
                    myGroup=miUser.groups.get()
                except : 
                    myGroup = 'none'
                
                ############ CAREFUL ##########
                # names of choices must be the same as model name if not it doesnt work !!
               
                if str(myGroup) == 'admin':                    
                    choices={'Cita','especialidad','sector','producto','personal','tratamiento','gastos','message'}
                else:
                    choices={'Cita','paciente','tratamiento','message'}
                #session_key = request.COOKIES[settings.SESSION_COOKIE_NAME]
                session_key = request.session.session_key

                ######################## django form #############################
                # https://docs.djangoproject.com/en/dev/topics/forms/modelforms/ #
                ##################################################################
                AuthorFormSet = modelformset_factory(Cita,extra=1)
                formset = AuthorFormSet(queryset=Cita.objects.none())
                
                AuthorFormSet2 = modelformset_factory(Paciente,extra=1)
                formset2 = AuthorFormSet2(queryset=Paciente.objects.none())
                
                direccion = DIRR
                
                
                c={'username':username,'group':myGroup,'choices':choices,'sessionId':session_key,'formset':formset,'formset2':formset2,'dirr':direccion}
                
                return render_to_response('tempAdmin.html', c, context_instance=RequestContext(request))
                return HttpResponse("kokok")
                ## ma page
            else:
                state = "Your account is not active, please contact the site admin."
                return render_to_response('auth.html',{'state':state, 'username': username})
        else:
            state = "Your username and/or password were incorrect."

    return render_to_response('auth.html',{'state':state, 'username': username})
    
@csrf_exempt
def FormCita(request):    
    
    AuthorFormSet = modelformset_factory(Cita,extra=1)
    formset = AuthorFormSet(queryset=Cita.objects.none())
    
    direccion = DIRR
    #session_key = request.COOKIES[settings.SESSION_COOKIE_NAME]
    session_key = request.session.session_key
    c={'formset':formset,'dirr':direccion,'sessionId':session_key}
    
    return render_to_response('formCita.html', c, context_instance=RequestContext(request))
   
    pass   